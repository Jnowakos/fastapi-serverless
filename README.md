# FASTapi-serverless

# Deploying 3scale on ROSA
https://mobb.ninja/docs/redhat/3scale/

# Example of knative deployment
apiVersion: serving.knative.dev/v1
kind: Service
metadata:
  name: hello 
  namespace: jason-test 
  labels:
    networking.knative.dev/visibility: cluster-local
spec:
  template:
    spec:
      containers:
        - image: registry.gitlab.com/jnowakos/fastapi-serverless:latest